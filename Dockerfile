FROM ubuntu:16.04

RUN apt-get update && \
    apt-get install -y software-properties-common && \
    apt-add-repository ppa:ansible/ansible

RUN apt-get update && \
    apt-get install -y ansible

CMD ["/usr/bin/ansible"]
